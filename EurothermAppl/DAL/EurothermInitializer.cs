﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using EurothermAppl.Models;
using System.Globalization;

namespace EurothermAppl.DAL
{
    public class EurothermInitializer : DropCreateDatabaseIfModelChanges<EurothermContext>
    {
        protected override void Seed(EurothermContext context)
        {
            var patients = new List<Patient>
            {
            new Patient{Name="Carson",Surname="Alexander",DateOfBirth=DateTime.ParseExact("1985/09/01", "yyyy/mm/dd", CultureInfo.InvariantCulture), Note="This is sample note"},
            new Patient{Name="Meredith",Surname="Alonso",DateOfBirth=DateTime.ParseExact("1987/09/01", "yyyy/mm/dd", CultureInfo.InvariantCulture), Note="This is sample note"},
            new Patient{Name="Arturo",Surname="Anand",DateOfBirth=DateTime.ParseExact("1987/09/01", "yyyy/mm/dd", CultureInfo.InvariantCulture), Note="This is sample note"},
            new Patient{Name="Gytis",Surname="Barzdukas",DateOfBirth=DateTime.ParseExact("1987/09/01", "yyyy/mm/dd", CultureInfo.InvariantCulture), Note="This is sample note"},
            new Patient{Name="Yan",Surname="Li",DateOfBirth=DateTime.ParseExact("1987/09/01", "yyyy/mm/dd", CultureInfo.InvariantCulture), Note="This is sample note"},
            new Patient{Name="Peggy",Surname="Justice",DateOfBirth=DateTime.ParseExact("1987/09/01", "yyyy/mm/dd", CultureInfo.InvariantCulture), Note="This is sample note"},
            new Patient{Name="Laura",Surname="Norman",DateOfBirth=DateTime.ParseExact("1987/09/01", "yyyy/mm/dd", CultureInfo.InvariantCulture), Note="This is sample note"},
            new Patient{Name="Nino",Surname="Olivetto",DateOfBirth=DateTime.ParseExact("1987/09/01", "yyyy/mm/dd", CultureInfo.InvariantCulture), Note="This is sample note"}
            };

            patients.ForEach(s => context.Patients.Add(s));
            context.SaveChanges();
        }
    }
}