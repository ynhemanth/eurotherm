﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegisterNewPatient.aspx.cs" Inherits="EurothermAppl.RegisterNewPatient" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function NameValidation(source, arguments) {
            if (arguments.Value != null) {
                arguments.IsValid = arguments.Value.length > 2 ? true : false;
            }
        }
        function SurnameValidation(source, arguments) {
            if (arguments.Value != null) {
                arguments.IsValid = arguments.Value.length > 3 ? true : false;
            }
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Create New Patient Details</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="Message" />
    </p>

    <div class="form-horizontal">
        <h4>Create a new patient record</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Name</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Name" ClientIDMode="Static" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Name"
                    CssClass="text-danger" ErrorMessage="The name field is required." />              
                <asp:CustomValidator ID="NameVal" runat="server" ClientValidationFunction="NameValidation" CssClass="text-danger"
                    ErrorMessage="Name must be more than 2 characters." ControlToValidate="Name" EnableClientScript="true"  /> 
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Surname" CssClass="col-md-2 control-label">Surname</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Surname" ClientIDMode="Static" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Surname"
                    CssClass="text-danger" ErrorMessage="The surname field is required." />
                <asp:CustomValidator ID="SurnameVal" runat="server" ClientValidationFunction="SurnameValidation" CssClass="text-danger"
                    ErrorMessage="Surname must be more than 3 characters." ControlToValidate="Surname" EnableClientScript="true"  /> 
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="DateOfBirth" CssClass="col-md-2 control-label">Date Of Birth</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="DateOfBirth" CssClass="form-control" placeholder="Accepted date format 'yyyy-mm-dd'" />
                  
                <asp:RequiredFieldValidator runat="server" ControlToValidate="DateOfBirth"
                    CssClass="text-danger" ErrorMessage="The DateOfBirth field is required." />
                 <asp:RegularExpressionValidator ID="rev" ControlToValidate="DateOfBirth" ValidationExpression="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$"
             runat="server" CssClass="text-danger" Style="margin-left:-20px;" ErrorMessage="Invalidate date format, only 'yyyy-mm-dd' date format is accepted"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Note" CssClass="col-md-2 control-label">Note</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Note" TextMode="MultiLine" CssClass="form-control" />                
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="btnSubmit" runat="server"  Text="Submit" CssClass="btn btn-primary" OnClick="btnSubmit_Click" />
            </div>
        </div>
    </div>
</asp:Content>
