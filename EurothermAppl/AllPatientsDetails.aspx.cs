﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EurothermAppl.DAL;

namespace EurothermAppl
{
    public partial class AllPatientsDetails : System.Web.UI.Page
    {
        EurothermContext context = new EurothermContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            AllPatientDetailsGridView.DataSource = context.Patients.Select(row => row).OrderBy(i => i.Surname).ToList();
            AllPatientDetailsGridView.DataBind();
        }
    }
}