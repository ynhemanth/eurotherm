﻿using EurothermAppl.DAL;
using EurothermAppl.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EurothermAppl
{
    public partial class RegisterNewPatient : System.Web.UI.Page
    {
        EurothermContext context = new EurothermContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (context.Patients.Count(i => i.Name.ToLower() == Name.Text.ToLower() && i.Surname.ToLower() == Surname.Text.ToLower()) == 0)
            {
                var setDoBVal = DateOfBirth.Text.Replace("-","/");
                DateTime DoB = DateTime.ParseExact(setDoBVal, "yyyy/mm/dd", CultureInfo.InvariantCulture);
                var newPatient = new Patient
                {
                    Name = Name.Text,
                    Surname = Surname.Text,
                    DateOfBirth = DoB,
                    Note = Note.Text
                };

                context.Patients.Add(newPatient);
                context.SaveChanges();
                Message.Text = "Patients all details have been successfully entered";
            }
            else
            {
                Message.Text = "Sorry, there is already a patient registered with these details";
            }
        }
    }
}