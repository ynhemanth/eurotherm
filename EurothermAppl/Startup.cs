﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EurothermAppl.Startup))]
namespace EurothermAppl
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
